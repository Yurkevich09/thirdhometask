
package thirdLessonHomeTask;

public class TaskFour {
    public static void main (String[] args){
        // Задача 4. Массив из 8 рандомных чисел от 1 до 10. Число с нечетным индексом заменить на ноль.
        System.out.println("Задача №4 ");
        int massiv4[] = new int [8];
        for (int i = 0; i < massiv4.length; i++){
            massiv4[i] = 1 + (int)(Math.random()*10);
            System.out.print(massiv4[i] + " ");
        }
        System.out.println(" ");
        for (int i = 0; i < massiv4.length; i++){
            if (i % 2 != 0){
                massiv4[i] = 0;
            }
            System.out.print(massiv4[i] + " ");
        }
    }
}
