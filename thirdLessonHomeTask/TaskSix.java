

package thirdLessonHomeTask;

public class TaskSix {
    public static void main (String[] args){
        // Задача 6. Создать сложный массив и вывести его на экран в табличном виде.
        // Вычислить сумму чисел в саммом длинном массиве и вывести на экран
        System.out.println("Задача №6 ");
        int massiv6[][] = new int [5][];
        int long_massiv = 0;
        int summ_long_massiv = 0;
        int number_long_massiv = 0;
        for (int i = 0; i < massiv6.length; i++){
            massiv6[i] = new int [i + 2];
            for (int j = 0; j < massiv6[i].length; j++){
                massiv6[i][j] = 1 + (int)(Math.random()*10);
                if (j % massiv6.length == massiv6[i].length - 1){
                    System.out.println(massiv6[i][j] + "\t");
                }
                else {
                    System.out.print(massiv6[i][j] + "\t");
                }
                if (long_massiv < massiv6[i].length){
                    number_long_massiv = i;
                    long_massiv = massiv6[i].length;
                }
            }
        }
        for (int i = 0; i < massiv6[number_long_massiv].length; i ++){
            summ_long_massiv = summ_long_massiv + massiv6[number_long_massiv][i];
        }
        System.out.println("\nСумма длинного массива: " + summ_long_massiv);
    }
}
