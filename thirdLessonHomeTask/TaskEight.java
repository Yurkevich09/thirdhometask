



package thirdLessonHomeTask;

public class TaskEight {
    public static void main (String[] args){
        // Задача 8. Вычислить сумму четных строк элементов сложного массива
        System.out.println("Задача №8 ");
        int massiv8[][] = new int [6][];
        int summ_massiv8[] = new int [massiv8.length];
        for (int i = 0; i < massiv8.length; i++){
            massiv8[i] = new int [i + 2];
            for (int j = 0; j < massiv8[i].length; j++){
                massiv8[i][j] = 1 + (int)(Math.random()*10);
                if (j % massiv8.length == massiv8[i].length - 1){
                    System.out.println(massiv8[i][j] + "\t");
                }
                else {
                    System.out.print(massiv8[i][j] + "\t");
                }
                summ_massiv8[i] = summ_massiv8[i] + massiv8[i][j];
            }
        }
        for (int i = 0; i < massiv8.length; i++){
            if (i % 2 != 0){
                System.out.print("\nНомер строки массива: " + (i+1)
                        + ", сумма массива: " + summ_massiv8[i]);
            }
        }
    }
}
